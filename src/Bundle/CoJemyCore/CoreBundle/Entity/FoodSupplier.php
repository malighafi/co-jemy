<?php

namespace Bundle\CoJemyCore\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class FoodSupplier
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $deliveryCost;

    /**
     * @var string
     */
    private $freeDeliveryThreshold;

    /**
     * @var string
     */
    private $singlePackageCost;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $websiteUrl;

    /**
     * @var string
     */
    private $menuUrl;

    /**
     * @var Collection
     */
    private $menuItems;

    public function __construct()
    {
        $this->menuItems = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return FoodSupplier
     */
    public function setName($name) : FoodSupplier
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $deliveryCost
     *
     * @return FoodSupplier
     */
    public function setDeliveryCost($deliveryCost) : FoodSupplier
    {
        $this->deliveryCost = $deliveryCost;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }

    /**
     * @param string $freeDeliveryThreshold
     *
     * @return FoodSupplier
     */
    public function setFreeDeliveryThreshold($freeDeliveryThreshold) : FoodSupplier
    {
        $this->freeDeliveryThreshold = $freeDeliveryThreshold;

        return $this;
    }

    /**
     * @return string
     */
    public function getFreeDeliveryThreshold()
    {
        return $this->freeDeliveryThreshold;
    }

    /**
     * @param string $singlePackageCost
     *
     * @return FoodSupplier
     */
    public function setSinglePackageCost($singlePackageCost) : FoodSupplier
    {
        $this->singlePackageCost = $singlePackageCost;

        return $this;
    }

    /**
     * @return string
     */
    public function getSinglePackageCost()
    {
        return $this->singlePackageCost;
    }

    /**
     * @param string $phoneNumber
     *
     * @return FoodSupplier
     */
    public function setPhoneNumber($phoneNumber) : FoodSupplier
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $websiteUrl
     *
     * @return FoodSupplier
     */
    public function setWebsiteUrl($websiteUrl) : FoodSupplier
    {
        $this->websiteUrl = $websiteUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }

    /**
     * @param string $menuUrl
     *
     * @return FoodSupplier
     */
    public function setMenuUrl($menuUrl) : FoodSupplier
    {
        $this->menuUrl = $menuUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getMenuUrl()
    {
        return $this->menuUrl;
    }

    /**
     * @param MenuItem $menuItem
     *
     * @return FoodSupplier
     */
    public function addMenuItem(MenuItem $menuItem) : FoodSupplier
    {
        $this->menuItems[] = $menuItem;

        return $this;
    }

    /**
     * @param MenuItem $menuItem
     */
    public function removeMenuItem(MenuItem $menuItem)
    {
        $this->menuItems->removeElement($menuItem);
    }

    /**
     * @return Collection
     */
    public function getMenuItems()
    {
        return $this->menuItems;
    }
}
