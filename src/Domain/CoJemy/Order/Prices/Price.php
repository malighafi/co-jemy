<?php

namespace Domain\CoJemy\Order\Prices;

class Price
{
    /**
     * @var Type
     */
    private $type;

    /**
     * @var int
     */
    private $amount;

    public function __construct(Type $type, $amount)
    {
        $this->type = $type;
        $this->amount = $amount;
    }

    /**
     * @return Type
     */
    public function getType() : Type
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getAmount() : int
    {
        return $this->amount;
    }
}
