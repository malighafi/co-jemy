# No to co jemy ?

### Co trzeba aby jeść ?
 - Docker
 - VirtualBox

### Jak jeść ?

Jeżeli nie masz aktywnej maszyny dockera:

```
$ docker-machine create co-jemy --driver virtualbox
```

```
$ eval "$(docker-machine env co-jemy)"
```

```
$ docker-machine ip co-jemy
```

```
$ brew install docker-machine-nfs
```

```
docker-machine-nfs your-docker-machine-name --shared-folder=/path-to-your-project-directory
--nfs-config="-mapall=your-username:staff 192.168.99.100"
--mount-opts="rw,async,noatime,rsize=32768,wsize=32768,proto=tcp,nfsvers=3" -f
```

Przechodzimy do katalogu z aplikacją.

```
$ docker-compose up -d --build web
```

Instalacja pakietów

```
$ docker-compose run web /bin/bash -c "composer install --no-interaction --optimize-autoloader"
```

Po zbudowaniu należy odpalić migracje.

```
$ docker-compose run web /bin/bash -c "composer dev"
```

Aplikacja powinna być dostępna pod adresem:

```
http://ip-maszyny/app_dev.php/
```

### Jak testować jedzenie ?

```
$ docker-compose run web bin/behat
```

```
$ docker-compose run web bin/phpspec run
```
